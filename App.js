import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Splash from "./src/screen/splash";
import Home from "./src/screen/home";
import AboutUs from "./src/screen/aboutUs";
import Contact from "./src/screen/contact";
import MyBooks from "./src/screen/myBooks";
// import ImportPdf from "./src/screen/importPdf";

const Stack = createStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AboutUs"
          options={{ title: "About Us" }}
          component={AboutUs}
        />
        <Stack.Screen name="Contact" component={Contact} />
        {/* <Stack.Screen name="ImportPdf" component={ImportPdf} /> */}
        <Stack.Screen name="MyBooks" component={MyBooks} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
