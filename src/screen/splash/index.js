import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Button } from "react-native";
import { CommonActions } from "@react-navigation/native";
import {
  requestReadPermission,
  requestWritePermission
} from "../../permission";
export class Splash extends Component {
  constructor(props) {
    super(props);
    this.onButtonPress = this.onButtonPress.bind(this);
  }

  componentDidMount() {
    requestReadPermission();
    requestWritePermission();
  }
  onButtonPress() {
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name: "Home" }]
      })
    );
  }

  render() {
    return (
      <View style={styles.splashView}>
        <Image
          style={styles.splashImage}
          source={require("../../assets/images/Group.png")}
        />
        <Text style={styles.appName}>Read Fast</Text>
        <Text style={styles.screenText}>
          Welcome to the Read fast , the only app which helps you to read faster
          , by perfectly syncing your EYES and EARS .
        </Text>
        <View style={styles.buttonContainer}>
          <Button
            color="#6C63FF"
            style={styles.homeBtn}
            title="Start reading"
            onPress={this.onButtonPress}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  splashImage: {
    height: 250,
    width: 250
  },
  splashView: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    color: "#1D1A4B",
    height: "100%"
  },
  appName: {
    fontSize: 48
  },
  screenText: {
    fontSize: 18,
    margin: 10,
    marginBottom: 25,
    textAlign: "center"
  },
  homeBtn: {
    backgroundColor: "#1D1A4B",
    borderRadius: 10
  },
  buttonContainer: {
    width: "60%",
    height: 48
  }
});
export default Splash;
