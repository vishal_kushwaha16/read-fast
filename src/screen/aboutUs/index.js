import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Button } from "react-native";

class AboutUs extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <Text style={styles.screenText}>
          Introducing READ-FAST the app which can increase your reading speed by
          upto 30% !!!
        </Text>
        <Text style={styles.screenText}>
          READ-FAST grabs your attention by syncing your Eyes and Ears in a
          perfect natural way .
        </Text>
        <Text style={styles.screenText}>
          Just copy your favourite text and paste it in the app , and then feel
          the magic .
        </Text>
        <Text style={styles.heading}>Features</Text>
        <Text style={styles.other}>
          * Just copy paste and start reading your favourite articles .
        </Text>
        <Text style={styles.other}>
          * Text to voice - to speed up your reading process .{" "}
        </Text>
        <Text style={styles.other}>
          {" "}
          * Change your speed and see how much time you have saved .
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenText: {
    fontSize: 20,
    padding: 10
  },
  heading: {
    fontSize: 20,
    padding: 10
  },
  other: {
    fontSize: 18,
    padding: 10
  }
});
export default AboutUs;
