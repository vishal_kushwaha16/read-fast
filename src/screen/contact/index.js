import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Button } from "react-native";

class Contact extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <Text style={styles.screenText}>Contact Us</Text>
        <Text style={styles.other}>
          Drop Us a Mail On : support@tdglabs.com
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenText: {
    fontSize: 24,
    padding: 10
  },
  other: {
    padding: 10
  }
});
export default Contact;
