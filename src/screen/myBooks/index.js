import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Button } from "react-native";

class MyBooks extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View >
        <Text style={styles.screenText}>
          My Books
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenText: {
    fontSize: 24,
    margin: 30
  },
});
export default MyBooks;
