import React, { Component } from "react";
import { TextInput, StyleSheet, ToastAndroid } from "react-native";
import TextToSpeech from "../../components/tts";
import TextBlock from "../../components/text-block";
import SwipeableList from "../../components/swipeableList";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: "",
      paragraphs: [],
      playingText: "",
      currentIndex: 0,
      initial: true,
      testWidth: "99%",
      textEditable: true,
      pdfFileData: ["please wait while we make thing happen."],
      isPdfSelected: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.finalText = this.finalText.bind(this);
    this.handleInitial = this.handleInitial.bind(this);
    this.startNew = this.startNew.bind(this);
    this.updatedFileData = this.updatedFileData.bind(this);
    this.showBlock = this.showBlock.bind(this);
    this.isPdfSelected = this.isPdfSelected.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ testWidth: "95%" });
    }, 100);
  }

  isPdfSelected(value) {
    this.setState({ isPdfSelected: value });
  }

  updatedFileData(file) {
    const formData = new FormData();
    formData.append("ebook", file);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "multipart/form-data");

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formData,
      redirect: "follow"
    };

    this.isPdfSelected(true);
    fetch("http://api-pdftotext.appspot.com", requestOptions)
      .then(response => response.text())
      .then(result => {
        const { text } = JSON.parse(result);
        let filterPages = [];
        let regex = /Break----------------\s*(.*?)\s*----------------Page /gi;
        let firstPage = JSON.stringify(text).split("----------------Page");
        filterPages.push(firstPage[0]);
        let filterText = JSON.stringify(text).match(regex);
        for (let i = 0; i < filterText.length; i++) {
          filterPages.push(
            filterText[i]
              .split("Break----------------")[1]
              .split("----------------Page")[0]
          );
        }
        this.setState({ pdfFileData: filterPages, userData: filterPages[0] });
        this.finalText();
      })
      .catch(error => {
        ToastAndroid.showWithGravity(
          "Something Went Wrong ! Please try again.",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        );
        this.finalText();
      });
  }

  handleChange(text) {
    this.setState({ userData: text });
  }

  handleInitial() {
    this.setState({ initial: false });
  }

  handleNext() {
    const { paragraphs, currentIndex } = this.state;
    if (paragraphs.length - 1 !== currentIndex) {
      this.setState({
        playingText: paragraphs[currentIndex + 1],
        currentIndex: currentIndex + 1
      });
    }
    return true;
  }

  handlePrevious() {
    const { paragraphs, currentIndex } = this.state;
    if (currentIndex > 0) {
      this.setState({
        playingText: paragraphs[currentIndex - 1],
        currentIndex: currentIndex - 1
      });
    }
    return true;
  }

  finalText() {
    const { userData } = this.state;
    if (userData) {
      const paraArray = userData.split(/\\r\\n|\n|\r/);
      if (paraArray.length === 1) {
        paraArray.push("");
      }
      this.setState({
        paragraphs: paraArray,
        playingText: paraArray[0],
        initial: false,
        textEditable: false
      });
    }
    return true;
  }

  startNew() {
    this.setState({
      userData: "",
      paragraphs: [],
      playingText: "",
      currentIndex: 0,
      initial: true,
      testWidth: "95%",
      textEditable: true,
      isPdfSelected: false
    });
  }

  onViewableItemsChanged = ({ viewableItems }) => {
    if (viewableItems.length > 0) {
      this.setState(
        prevState => {
          return { userData: prevState.pdfFileData[viewableItems[0].index] };
        },
        function() {
          this.finalText();
        }
      );
    }
  };

  showBlock() {
    const {
      initial,
      isPdfSelected,
      pdfFileData,
      paragraphs,
      currentIndex
    } = this.state;
    if (initial && !isPdfSelected) {
      return (
        <TextInput
          multiline={true}
          placeholder="Type here to translate!"
          style={{
            width: this.state.testWidth,
            margin: 10,
            borderColor: "#000",
            borderWidth: 1,
            textAlignVertical: "top",
            height: "80%",
            borderRadius: 10
          }}
          onChangeText={this.handleChange}
          value={this.state.text}
          editable={this.state.textEditable}
        />
      );
    } else if (isPdfSelected) {
      return (
        <SwipeableList
          pages={pdfFileData}
          style={{
            margin: 10,
            borderColor: "#000",
            borderWidth: 1,
            height: "80%"
          }}
          onViewableItemsChanged={this.onViewableItemsChanged}
        />
      );
    }
    return (
      <TextBlock
        paragraphs={paragraphs}
        currentIndex={currentIndex}
        style={{
          margin: 10,
          borderColor: "#000",
          borderWidth: 1,
          width: "95%",
          height: "95%",
          borderRadius: 10
        }}
      />
    );
  }

  render() {
    const { initial, currentIndex, paragraphs } = this.state;
    return (
      <>
        {this.showBlock()}
        <TextToSpeech
          text={this.state.playingText}
          handleNext={this.handleNext}
          handlePrevious={this.handlePrevious}
          finalText={this.finalText}
          handleInitial={this.handleInitial}
          navigation={this.props.navigation}
          startNew={this.startNew}
          currentIndex={currentIndex}
          initial={initial}
          updatedFileData={this.updatedFileData}
          isPdfSelected={this.isPdfSelected}
          paragraphs={paragraphs}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1
  },
  controls: {
    flex: 0.4
  },
  playButton: {
    alignItems: "center",
    width: "100%",
    height: "100%"
  }
});

export default Home;
