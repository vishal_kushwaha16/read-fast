import React, { Component } from "react";
import DocumentPicker from "react-native-document-picker";
import { ToastAndroid } from "react-native";

export const filePicker = async updatedFileData => {
  try {
    const res = await DocumentPicker.pick({
      type: [DocumentPicker.types.pdf]
    });
    var file;
    file = {
      uri: res.uri,
      type: res.type,
      name: res.name
    };
    updatedFileData(file);
    ToastAndroid.showWithGravity(
      "Please Wait Will We Are Making Thing Readable.",
      ToastAndroid.LONG,
      ToastAndroid.CENTER
    );
  } catch (err) {
    if (DocumentPicker.isCancel(err)) {
    } else Toast.show("The file could not be uploaded.", 3000);
  }
};

// export default ImportPdf;
