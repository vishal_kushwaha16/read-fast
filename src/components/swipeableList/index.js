import React from "react";
import {
  FlatList,
  View,
  Text,
  TouchableOpacity,
  Dimensions
} from "react-native";

function Item({ title }) {
  return (
    <View
      style={{
        width: Dimensions.get("screen").width - 20
      }}
    >
      <Text>{title}</Text>
    </View>
  );
}

class SwipeableList extends React.Component {
  _viewabilityConfig = {
    itemVisiblePercentThreshold: 50
  };
  render() {
    return (
      <View>
        <FlatList
          onViewableItemsChanged={this.props.onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
          data={this.props.pages}
          renderItem={({ item }, index) => <Item id={index} title={item} />}
          keyExtractor={item => item}
          horizontal
          numColumns={1}
          style={this.props.style}
          snapToInterval={Dimensions.get("screen").width - 20}
          decelerationRate={0.0}
          showsHorizontalScrollIndicator={false}
          removeClippedSubviews
          bounces
          pagingEnabled
        />
      </View>
    );
  }
}

export default SwipeableList;
