import React from "react";
import { ScrollView, Text, StyleSheet, View } from "react-native";

function TextBlock({ paragraphs, currentIndex, style }) {
  return (
    <ScrollView contentContainerStyle={style}>
      {paragraphs.map((para, index) => {
        if (currentIndex === index) {
          return (
            <Text
              style={{
                ...styles.highlight,
                ...{
                  borderTopLeftRadius: index === 0 ? 10 : 0,
                  borderTopRightRadius: index === 0 ? 10 : 0
                }
              }}
              key={index}
            >
              {para}
            </Text>
          );
        }
        return <Text key={index}>{para}</Text>;
      })}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    //height: 680,
    width: 385,
    margin: 5,
    borderWidth: 1,
    borderRadius: 5
  },

  highlight: {
    backgroundColor: "lightblue"
  }
});

export default TextBlock;
