import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  AppState
} from "react-native";
import Tts from "react-native-tts";
import Modal, { ModalContent, SlideAnimation } from "react-native-modals";
import images from "../../constants/images";
import { filePicker } from "../importPdf";

class TextToSpeech extends React.Component {
  constructor() {
    super();
    this.state = {
      voices: [],
      ttsStatus: "initializing",
      selectedVoice: null,
      speechRate: 0.5,
      speechPitch: 1,
      isPlaying: false,
      speedIndex: 1,
      showModal: false,
      appState: AppState.currentState
    };
    this.toggleSpeed = this.toggleSpeed.bind(this);
    this.showModal = this.showModal.bind(this);
    this.navigateContact = this.navigateContact.bind(this);
    this.navigateAboutUs = this.navigateAboutUs.bind(this);
    // this.navigateImportPdf = this.navigateImportPdf.bind(this);
    this.navigateMyBooks = this.navigateMyBooks.bind(this);
    this.startNew = this.startNew.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);

    Tts.addEventListener("tts-start", event =>
      this.setState({ ttsStatus: "started", isPlaying: true })
    );
    Tts.addEventListener("tts-finish", event => {
      {
        this.setState({ ttsStatus: "finished", isPlaying: false });
        this.handleNext();
      }
    });
    Tts.addEventListener("tts-cancel", event =>
      this.setState({ ttsStatus: "cancelled", isPlaying: false })
    );
    Tts.setDefaultRate(this.state.speechRate);
    Tts.setDefaultPitch(this.state.speechPitch);
    Tts.getInitStatus().then(this.initTts);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { currentIndex, initial } = this.props;
    if (currentIndex !== nextProps.currentIndex && !initial) {
      this.readMore(nextProps.text, true);
    }
    if (
      initial !== nextProps.initial &&
      currentIndex === nextProps.currentIndex
    ) {
      this.readText(nextProps.text);
    }
    return true;
  }

  _handleAppStateChange = nextAppState => {
    if (nextAppState === "background") Tts.stop();
    //    if (nextAppState === "active") this.readText(this.props.text);
  };

  handleNext() {
    this.props.handleNext();
    this.setState({ isPlaying: false });
  }

  initTts = async () => {
    const voices = await Tts.voices();
    const availableVoices = voices
      .filter(v => !v.networkConnectionRequired && !v.notInstalled)
      .map(v => {
        return { id: v.id, name: v.name, language: v.language };
      });
    let selectedVoice = null;
    if (voices && voices.length > 0) {
      selectedVoice = voices[0].id;
      try {
        await Tts.setDefaultLanguage(voices[0].language);
      } catch (err) {
        // My Samsung S9 has always this error: "Language is not supported"
        console.log(`setDefaultLanguage error `, err);
      }
      await Tts.setDefaultVoice(voices[0].id);
      this.setState({
        voices: availableVoices,
        selectedVoice,
        ttsStatus: "initialized"
      });
    } else {
      this.setState({ ttsStatus: "initialized" });
    }
  };

  readMore = async (text, speedControl) => {
    Tts.stop();
    if (this.state.isPlaying && !speedControl) {
      Tts.stop();
      this.setState({ isPlaying: false });
    } else {
      this.setState({ isPlaying: true });
      Tts.speak(text, {
        androidParams: {
          KEY_PARAM_PAN: -1,
          KEY_PARAM_VOLUME: 0.5,
          KEY_PARAM_STREAM: "STREAM_MUSIC"
        }
      });
    }
  };

  readText = async (text, speedControl) => {
    Tts.stop();
    if (this.state.isPlaying && !speedControl) {
      Tts.stop();
    } else {
      Tts.speak(text, {
        androidParams: {
          KEY_PARAM_PAN: -1,
          KEY_PARAM_VOLUME: 0.5,
          KEY_PARAM_STREAM: "STREAM_MUSIC"
        }
      });
      this.setState({ isPlaying: true });
    }
  };

  convertText = () => {
    this.props.finalText();
  };

  setSpeechRate = async rate => {
    await Tts.setDefaultRate(rate);
    this.setState({ speechRate: rate });
  };

  setSpeechPitch = async rate => {
    await Tts.setDefaultPitch(rate);
    this.setState({ speechPitch: rate });
  };

  onVoicePress = async voice => {
    try {
      await Tts.setDefaultLanguage(voice.language);
    } catch (err) {
      // My Samsung S9 has always this error: "Language is not supported"
      console.log(`setDefaultLanguage error `, err);
    }
    await Tts.setDefaultVoice(voice.id);
    this.setState({ selectedVoice: voice.id });
  };

  async toggleSpeed() {
    const speed = [0.25, 0.5, 0.75, 1];
    const { speedIndex } = this.state;
    if (speedIndex < 3) {
      this.setState({
        speechRate: speed[speedIndex + 1],
        speedIndex: speedIndex + 1,
        isPlaying: true
      });
      await Tts.setDefaultRate(speed[speedIndex + 1]);
    } else {
      this.setState({
        speechRate: speed[0],
        speedIndex: 0,
        isPlaying: true
      });
      await Tts.setDefaultRate(speed[0]);
    }
    if (this.props.text) {
      Tts.stop();
      this.readText(this.props.text, true);
    }
  }
  showModal() {
    this.setState(previousState => {
      return { showModal: !previousState.showModal };
    });
  }

  navigateAboutUs() {
    this.props.navigation.navigate("AboutUs");
    this.showModal();
  }

  navigateContact() {
    this.props.navigation.navigate("Contact");
    this.showModal();
  }

  navigateMyBooks() {
    this.props.navigation.navigate("MyBooks");
    this.showModal();
  }

  startNew() {
    this.props.startNew();
    this.props.isPdfSelected(false);
  }
  render() {
    const { handleNext, handlePrevious, initial, text } = this.props;
    const { showModal } = this.state;
    const speed = this.state.speechRate * 2;
    return (
      <>
        <View style={styles.speed}>
          <TouchableOpacity
            onPress={this.toggleSpeed}
            disabled={text ? false : true}
          >
            <Text style={styles.read}>{speed}X</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.flexbox}>
          <TouchableOpacity
            onPress={this.showModal}
            style={styles.read}
            accessible={true}
            accessibilityLabel="Menu"
          >
            <Image source={images.MenuIcon} style={styles.icons} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={handlePrevious}
            style={styles.read}
            accessible={true}
            accessibilityLabel="Previous Paragraph"
          >
            <Image source={images.RewindIcon} style={styles.play} />
          </TouchableOpacity>
          {initial ? (
            <TouchableOpacity
              onPress={this.convertText}
              style={styles.read}
              accessible={true}
              accessibilityLabel="Play Audio"
            >
              <Image source={images.PlayAudioIcon} style={styles.play} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => this.readText(this.props.text)}
              style={styles.read}
              accessible={true}
              accessibilityLabel="Play/Pause Audio"
            >
              <Image
                source={
                  !this.state.isPlaying
                    ? images.PlayAudioIcon
                    : images.PauseAudioIcon
                }
                style={styles.play}
              />
            </TouchableOpacity>
          )}
          <TouchableOpacity
            onPress={handleNext}
            style={styles.read}
            accessible={true}
            accessibilityLabel="Next Paragraph"
          >
            <Image source={images.FastFwdIcon} style={styles.play} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconsTouchArea}
            onPress={this.startNew}
          >
            <Image
              source={images.clean}
              style={styles.icons}
              accessible={true}
              accessibilityLabel="New"
            />
          </TouchableOpacity>
          <Modal.BottomModal
            visible={showModal}
            rounded
            actionsBordered
            swipeDirection={["down"]} // can be string or an array
            onSwipeOut={event => {
              this.setState({ showModal: false });
            }}
            modalAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ showModal: false });
            }}
          >
            <ModalContent>
              {/* <TouchableOpacity
                style={styles.optionsRow}
                
              >
                <Image
                  resizeMode={"contain"}
                  source={images.NewFileIcon}
                  style={styles.optionsIcon}
                />
                <Text style={styles.optionsText}>New File</Text>
              </TouchableOpacity> */}
              <TouchableOpacity
                style={styles.optionsRow}
                onPress={() => {
                  this.showModal();
                  filePicker(this.props.updatedFileData);
                }}
              >
                <Image
                  resizeMode={"contain"}
                  source={images.ImportPdfIcon}
                  style={styles.optionsIcon}
                />
                <Text style={styles.optionsText}>Import PDF</Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={styles.optionsRow}
                onPress={this.navigateMyBooks}
              >
                <Image
                  resizeMode={"contain"}
                  source={images.MyBooksIcon}
                  style={styles.optionsIcon}
                />
                <Text style={styles.optionsText}>My Books</Text>
              </TouchableOpacity> */}
              <TouchableOpacity
                style={styles.optionsRow}
                onPress={this.navigateContact}
              >
                <Image
                  resizeMode={"contain"}
                  source={images.ContactIcon}
                  style={styles.optionsIcon}
                />
                <Text style={styles.optionsText}>Contact Us</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.optionsRow}
                onPress={this.navigateAboutUs}
              >
                <Image
                  resizeMode={"contain"}
                  source={images.AboutIcon}
                  style={styles.optionsIcon}
                />
                <Text style={styles.optionsText}>About Us</Text>
              </TouchableOpacity>
            </ModalContent>
          </Modal.BottomModal>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  flexbox: {
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%"
  },
  play: {
    height: 50,
    width: 50
  },
  speed: {
    alignItems: "center",
    marginBottom: 10
  },
  read: {
    fontSize: 18
  },
  iconsTouchArea: {
    height: 48,
    width: 48
  },
  icons: {
    height: 36,
    width: 36,
    marginTop: 10
  },
  optionsRow: {
    flexDirection: "row",
    justifyContent: "flex-start",
    margin: 10
  },
  optionsIcon: {
    width: 25,
    height: 25
  },
  optionsText: {
    marginLeft: 10,
    fontSize: 18
  }
});
export default TextToSpeech;
