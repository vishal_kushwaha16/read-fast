export default {
  AboutIcon: require(".././assets/images/Icon_material-people.png"),
  ContactIcon: require(".././assets/images/Icon_material-call.png"),
  NewFileIcon: require(".././assets/images/new_file.png"),
  RewindIcon: require(".././assets/images/mdi_fast_rewind.png"),
  PlayAudioIcon: require(".././assets/images/Vector.png"),
  PauseAudioIcon: require(".././assets/images/mdi_pause_circle_outline.png"),
  FastFwdIcon: require(".././assets/images/fast_forward.png"),
  MenuIcon: require(".././assets/images/mdi_more_vert.png"),
  ImportPdfIcon: require(".././assets/images/pdf.png"),
  MyBooksIcon: require(".././assets/images/book.png"),
  SettingsIcon: require(".././assets/images/mdi_settings.png"),
  clean: require("../assets/images/clean.png")
};
